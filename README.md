<h4>Run the bot:</h4>
Ensure <i>nodejs</i> is installed. Then:
<pre>
git clone git://gitlab.com/leonardolandi/telegram-perudobot.git
cd telegram-perudobot
npm install
</pre>
Then open <i>server.js</i> with a text editor and change the first line, depending on your TOKEN assigned by BotFather.<br>
Start the server with:
<pre>
node server.js
</pre>
