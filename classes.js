// classes.js
// This file is part of @Perudobot

// Copyright (C) 2016 Leonardo Landi

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


function Player(id,name,num) {
	this.id = id;
	this.username = name;
	this.dices = [];
	for (var i=0; i<num; i++) { this.dices.push(0) }
}

function Game(DICES,MAX_PLAYERS,BET_TIME) {
	this.DICES = DICES;
	this.MAX_PLAYERS = MAX_PLAYERS;
	this.BET_TIME  = BET_TIME*1000;
	this.playing = false;
	this.waiting_palific = false;
	this.palific = false;
	this.players = [];
	this.current = 0;
	this.bet = [1,0];
	this.timeout = null;
	this.ready = 0;
	this.bot_blocked = [];
}

Game.prototype.new_order = function() {
	var p = this.players;
	for (var j, x, i = p.length; i; j = parseInt(Math.random() * i), x = p[--i], p[i] = p[j], p[j] = x);
	this.players = p;
}

Game.prototype.get_order = function() {
	var players = [];
	for (var i=this.current; i<this.current + this.players.length; i++) { players.push(this.players[i%this.players.length]) }
	this.players = players;
	this.current = 0;
	var str = "";
	for (var i=0; i<this.players.length; i++) { str += "<b>" + (i+1).toString() + "</b>. " + this.players[i].username + " (" + this.players[i].dices.length + ")\n" }
	return str;
}

Game.prototype.roll_dices = function() {
	for (var i in this.players) {
		var length = this.players[i].dices.length;
		this.players[i].dices = [];
		for (var j=0; j<length; j++) { this.players[i].dices.push(Math.floor(Math.random() * 6) + 1) }
		this.players[i].dices.sort();
	}
}

Game.prototype.validate = function(newbet) {
	var oldbet = this.bet;
	if (newbet[0] < 1) { return 1 }	// number of dices must not be < 1
	else if (newbet[1] < 1 || newbet[1] > 6) { return 2 } // faces are between 1 and 6
	else if (newbet[1] != oldbet[1] && oldbet[1] > 0 && this.palific) { return 3 } // palific requests same face
	else if (newbet[0] <= oldbet[0] && oldbet[1] > 0 && this.palific) { return 4 } // must increment number in palific
	else if (newbet[1] == 1 && oldbet[1] == 0 && !this.palific) { return 5 } // cannot start with a 1 if not palific
	else if (newbet[1] == 1 && oldbet[1] > 1 && 2*newbet[0] < oldbet[0]) { return 6 } // convert to 1
	else if (newbet[1] > 1 && oldbet[1] == 1 && newbet[0] < 2*oldbet[0] + 1) { return 7 } // convert from 1
	else if (newbet[1] > 1 && oldbet[0] > newbet[0]) { return 8 } // new number must not be less than old
	else if (newbet[1] > 1 && oldbet[0] == newbet[0] && oldbet[1] >= newbet[1]) { return 8 } // if equals, must increment face
	else if (newbet[1] == 1 && oldbet[1] == 1 && oldbet[0] >= newbet[0]) { return 9 } // must increment from 1 to 1
	else { return 0 }
}

Game.prototype.check = function() {
	var number = 0;
	for (var i in this.players) {
		var hand = this.players[i].dices;
		if (this.palific) { for (var j in hand) { if (hand[j] == this.bet[1]) { number += 1 } } }
		else { for (var j in hand) { if (hand[j] == this.bet[1] || hand[j] == 1) { number += 1 } } }
	}
	return number;
}

exports.Player = Player;
exports.Game = Game;
