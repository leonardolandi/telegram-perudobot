// server.js
// This file is part of @Perudobot

// Copyright (C) 2016 Leonardo Landi

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


// INCLUDE THESE LINES FOR SERVER HTTP REQUESTS

//var express = require("express")
//var app = express()
//app.set("port", (process.env.PORT || 5000))
//app.use(express.static(__dirname + "/public"))
//app.get("/", function(request, response) { response.send("PerudoBot is running") })
//app.listen(app.get("port"), function() {})

// END OF LINES

var TOKEN = "";
var TELEGRAM_BOT = require("telegram-bot-api");
var BOT = new TELEGRAM_BOT({ token: TOKEN, updates: { enabled: true } });
var fs = require("fs");
var GAME = require("./classes.js");

var ICONS = { 1: "\u0031\u20e3", 2: "\u0032\u20e3", 3: "\u0033\u20e3", 4: "\u0034\u20e3", 5: "\u0035\u20e3", 6: "\u0036\u20e3" }
var Games = {}

function validate_number(num) {	// check if a number argument is correct
	var chars = ["0","1","2","3","4","5","6","7","8","9"];
	if (num.length == 0) { return false }
	else if (num[0] == "0" && num.length > 1) { return false }
	else {
		for (i in num) { if (chars.indexOf(num[i]) < 0) { return false } }
		return true;
	}
}

BOT.on("message", function(message) {
	if (message == undefined) { return }
	var answer = ON_COMMAND(message.chat.id, message.from.id, message.from.first_name, message.text, message.chat.type, false);
	if (answer !== undefined) { BOT.sendMessage({ chat_id: message.chat.id, text: "\u26d4 <i>" + message.from.first_name + ": " + answer + "</i>", parse_mode: "HTML" }).then(function(data){}).catch(function(error){}) }
});

BOT.on("inline.callback.query", function(message) {
	if (message == undefined) { return }
	var answer = ON_COMMAND(message.message.chat.id, message.from.id, message.from.first_name, message.data, message.message.chat.type,true);
	if (answer == undefined) { BOT.answerCallbackQuery({ callback_query_id: message.id }).then(function(data){}).catch(function(error){}) }
	else { BOT.answerCallbackQuery({ callback_query_id: message.id, text: answer }).then(function(data){}).catch(function(error){}) }
});

function ON_COMMAND(chat,id,name,text,type,is_answer) {

	function SEND(text,keyboard) { // send a text to the public chat
		if (keyboard == undefined) { BOT.sendMessage({ chat_id: chat, text: text, parse_mode: "HTML" }) }
		else { BOT.sendMessage({ chat_id: chat, text: text, parse_mode: "HTML", reply_markup: JSON.stringify({ inline_keyboard: keyboard }) }).then(function(data){}).catch(function(error){}) }
	}

	function send_dices(from,args) { // send dices to all players in private chat
		Games[chat].ready = 0;
		Games[chat].bot_blocked = [];
		Games[chat].bet = [1,0];
		Games[chat].roll_dices();
		for (var i in Games[chat].players) {
			var icons = "", player = Games[chat].players[i];
			for (var j in player.dices) { icons += ICONS[player.dices[j]] }
			BOT.sendMessage({ chat_id: player.id, text: _("Your dices") + ": " + icons }).then( function(data) {
				Games[chat].ready++;
				if (Games[chat].ready == Games[chat].players.length) { new_turn(from,args) }
			}).catch( function(error) {
				if (error.error !== undefined && error.error.error_code == 403) {
					Games[chat].ready++;
					Games[chat].bot_blocked.push(error.options.formData.chat_id);
					if (Games[chat].ready == Games[chat].players.length) { new_turn(from,args) }
				}
			});
		}
	}

	function new_turn(from,args) { // check if all players received dices or if someone has blocked the bot
		var init_str = "", middle_str = "", end_str = "";
		if (args !== undefined && args.str !== undefined) { init_str = args.str }
		if (Games[chat].bot_blocked.length > 0) {
			// Remove from the game players who blocked the bot and get their names
			var names = "";
			for (var i=Games[chat].players.length-1; i>-1; i--) {
				if (Games[chat].bot_blocked.indexOf(Games[chat].players[i].id) > -1) {
					names += Games[chat].players[i].username + ", ";
					if (Games[chat].current >= i) { Games[chat].current = (Games[chat].current - 1 + Games[chat].players.length) % Games[chat].players.length }
					Games[chat].players.splice(i,1);
				}
			}
			names = names.slice(0,-2);
			// Create a message of players who blocked the bot to be appended in the middle
			middle_str += "<b>" + names + "</b> ";
			if (Games[chat].bot_blocked.length == 1) { middle_str += _("has to leave the game because he can't receive dices") }
			else { middle_str += _("have to leave the game because they can't receive dices") }
			middle_str += "\n\n\u26a0 <i>" + names + ": " + _("Start @Perudobot in a private chat to receive dices. Click here") + ":</i> <a href='@Perudobot'>@Perudobot</a>. <i>" + _("You will be able to join the next game") + "</i>\n\n";
		}
		// Create the beginning of the message and set the end of the turn, depending on the cause
		if (from == "newgame_timeout" || from == "newgame_maxplayers") {
			Games[chat].playing = true;
			Games[chat].new_order();
			if (from == "newgame_timeout") { init_str = _("A new game has started") + "\n\n" }
			else { init_str = "<b>" + args.username + "</b> " + _("joined the game") + ". " + _("A new game has started") + "\n\n" } }
		else if (from == "palifico") {
			Games[chat].waiting_palific = false;
			Games[chat].palific = true;
			init_str = "<b>" + args.username + "</b> " + _("has chosen Palifico") + "\n\n" }
		else if (from == "nopalifico") {
			Games[chat].waiting_palific = false;
			Games[chat].palific = false;
			init_str = "<b>" + args.username + "</b> " + _("has not chosen Palifico") + "\n\n" }
		else if (from == "timeout_palifico") {
			Games[chat].waiting_palific = false;
			Games[chat].palific = false;
			init_str = "<b>" + args.username + "</b> " + _("runs out of time to decide and automatically chooses Not Palifico") + "\n\n" }
		else { Games[chat].palific = false }
		// Create the end of the message depending on the number of players left
		if (Games[chat].players.length == 0) {
			end_str = _("There are no players left in the game. The game ends");
			clearTimeout(Games[chat].timeout);
			SEND(init_str + middle_str + end_str,[[{ text: _("New game"), callback_data: "/startgame" }]]);
			delete(Games[chat]) }
		else if (Games[chat].players.length == 1) {
			end_str = "<b>" + Games[chat].players[0].username + "</b> " + _("is the winner!");
			clearTimeout(Games[chat].timeout);
			SEND(init_str + middle_str + end_str,[[{ text: _("New game"), callback_data: "/startgame" }]]);
			delete(Games[chat]) }
		else {
			end_str = _("The order of players is") + ":\n"+ Games[chat].get_order() + "\n" + _("Dices are dealt. It's the turn of") + " <b>" + Games[chat].players[0].username + "</b>";
			startTimer();
			SEND(init_str + middle_str + end_str) }
	}

	function player_leaves(ind,str) { // a player leaves a game
		Games[chat].players.splice(ind,1);
		var l = Games[chat].players.length;
		if (l == 1) {
			SEND(str + "\n\n<b>" + Games[chat].players[0].username + "</b> " + _("is the winner!"),[[{ text: _("New game"), callback_data: "/startgame" }]]);
			clearTimeout(Games[chat].timeout);
			delete Games[chat] }
		else {
			if (Games[chat].current >= ind) { Games[chat].current = (Games[chat].current - 1 + l) % l }
			send_dices("leave", { str: str + "\n\n" });
		}
	}

	function startTimer() { // countdown is started
		if (Games[chat].BET_TIME > 0) {
			Games[chat].timeout = setTimeout( function() { player_leaves(Games[chat].current,"<b>" + Games[chat].players[Games[chat].current].username + "</b> " + _("runs out of time and must leave the game")) }, Games[chat].BET_TIME);
		}
	}

	function _(string) { // translate
		var lang = JSON.parse(fs.readFileSync("groups.json"))[chat].language;
		if (lang == "en") { return string }
		var output = JSON.parse(fs.readFileSync("LANG/" + lang + ".json"))["\"" + string + "\""];
		if (output.length == 0) { return string }
		return output;
	}

//	STORES THE CHAT PARAMETERS, IF NOT EXIST
	var groups = JSON.parse(fs.readFileSync("groups.json").toString());
	if (groups[chat] == undefined) {
		groups[chat] = { language: "en", maxplayers: 6, dices: 5, timeout: 0 }
		fs.writeFileSync("groups.json",JSON.stringify(groups));
	}

//	PREVENTS FROM CRASH
	if (text == undefined) { return }

//	BOT IS NEWLY ADDED TO A CHAT OR RESTARTED
	else if (text == "/start" || text == "/start@Perudobot" || text == "/about" || text == "/about@Perudobot") {
		var str = _("Hi, I'm Perudobot, a Telegram bot for the game of Perudo") + "\n\n";
		if (type != "private") { str += "\u26a0 " + _("If this is your first time, remember that you have to keep open a private chat with me, making sure that I'm not blocked. Click here") + ": <a href='@Perudobot'>@Perudobot</a>\n\n" }
		str += _("You may want to <b>change the language</b> or <b>edit settings</b>. Click the button below") + "\n\n" + _("Enjoy!");
		if (type == "private") { SEND(str,keyboard = [[{ text: _("Settings"), callback_data: "/settings" }, { text: _("Source code"), url: "https://gitlab.com/leonardolandi/telegram-perudobot/" }]]) }
		else { SEND(str,[[{ text: _("New game"), callback_data: "/startgame" }],[{ text: _("Settings"), callback_data: "/settings" }, { text: _("Source code"), url: "https://gitlab.com/leonardolandi/telegram-perudobot/" }]]) }
		return;
	}

//	SETTINGS COMMAND
	else if (text == "/settings" || text == "/settings@Perudobot") {
		fs.readFile("groups.json", function(error,content) {
			var group = JSON.parse(content)[chat];
			var str = _("Use <code>set</code> command like this") + ":\n\n";
			str += "\u25aa <b>/set language xy</b>: " + _("Change the language (where xy is the 2-letters code of the language)") + "\n";
			str += "\u25aa <b>/set dices x</b>: " + _("Set the initial number of dices owned by each player") + " (default x=" + group.dices.toString() + ")\n";
			str += "\u25aa <b>/set players x</b>: " + _("Set the maximum number of players in a single game") + " (default x=" + group.maxplayers.toString() + ")\n";
			str += "\u25aa <b>/set timeout x</b>: " + _("Set the maximum time in seconds to make a bet") + " (default x=" + group.timeout.toString() + ", 0: " + _("no limits") + ")\n\n";
			str += _("Changes will only affect this chat and no others");
			SEND(str);
		});
		return;
	}

//	SET OPTIONS
	else if (text.substring(0,4) == "/set") {
		var arr = text.split(" ");
		if (arr[0] != "/set") { return _("Invalid command") }
		if (arr.length == 1) { return _("Invalid command") }
		var options = ["language","dices","players","timeout"];
		var ind = options.indexOf(arr[1]);
		if (ind < 0) { return "'" + arr[1] + "' " + _("isn't a valid option") }
		if (arr.length == 2) { return _("Missing argument after") + " '" + options[ind] + "'" }
		if (arr.length > 3) { return _("Too much arguments after") + " '" + options[ind] + "'" }
		if (ind > 0 && !validate_number(arr[2])) { return _("Invalid argument after") + " '" + options[ind] + "'" }
		if (ind == 0 && arr[2].length != 2) { return _("Invalid language format") }
		if (ind == 0) {
			var languages = fs.readdirSync("./LANG/");
			for (var i in languages) { languages[i] = languages[i].substr(0,2) }
			if (languages.indexOf(arr[2]) < 0) { return _("Language not available") }
			fs.readFile("groups.json",function(error,content) {
				var cont = JSON.parse(content);
				cont[chat].language = arr[2];
				fs.writeFile("groups.json",JSON.stringify(cont),function(err){});
			});
			SEND("\u2705 <b>" + name + "</b> " + _("changed the language to") + " <b>" + arr[2] + "</b>");
			return }
		var value = parseInt(arr[2]);
		if (ind == 1 && value < 1) { return _("There must be one dice at least") }
		if (ind == 1) {
			fs.readFile("groups.json",function(error,content) {
				var cont = JSON.parse(content);
				cont[chat].dices = value;
				fs.writeFile("groups.json",JSON.stringify(cont),function(err){});
			});
			SEND("\u2705 <b>" + name + "</b> " + _("set the number of dices to") + " <b>" + value.toString() + "</b>. " + _("Changes will be applied the next game"));
			return }
		if (ind == 2 && value < 2) { return _("There must be two players at least") }
		if (ind == 2) {
			fs.readFile("groups.json",function(error,content) {
				var cont = JSON.parse(content);
				cont[chat].maxplayers = value;
				fs.writeFile("groups.json",JSON.stringify(cont),function(err){});
			});
			SEND("\u2705 <b>" + name + "</b> " + _("set the maximum number of players to") + " <b>" + value.toString() + "</b>. " + _("Changes will be applied the next game"));
			return }
		fs.readFile("groups.json",function(error,content) {
			var cont = JSON.parse(content);
			cont[chat].timeout = value;
			fs.writeFile("groups.json",JSON.stringify(cont),function(err){});
		});
		var str = "\u2705 <b>" + name + "</b> ";
		if (value > 0) { str += _("set the timeout to") + " <b>" + value.toString() + "</b> " + _("seconds") }
		else { str += _("removed the timeout") }
		SEND(str + ". " + _("Changes will be applied the next game"));
		return;
	}

//	NEWGAME COMMAND
	else if (text == "/newgame" || text == "/newgame@Perudobot") {
		if (Games[chat] !== undefined && Games[chat].playing) { return _("There is already an existing game") }
		if (Games[chat] !== undefined) { return _("There is already an existing game. You can join it") }
		SEND(_("Click here to start a new game"),[[{ text: _("New game"), callback_data: "/startgame" }]]);
		return;
	}

//	STARTGAME COMMAND (ONLY FROM BUTTON)
	else if (is_answer && (text == "/startgame" || text == "/startgame@Perudobot")) {
		if (Games[chat] !== undefined && Games[chat].playing) { return "\u26d4 " + _("There is already an existing game") }
		if (Games[chat] !== undefined) { return "\u26d4 " + _("There is already an existing game. You can join it") }
		SEND("<b>" + name + "</b> " + _("has started a new game. Players have 30 seconds to join"),[[{ text: _("Join the game"), callback_data: "/join" }]]);
		var group = JSON.parse(fs.readFileSync("groups.json").toString())[chat];
		Games[chat] = new GAME.Game(group.dices,group.maxplayers,group.timeout);
		Games[chat].players.push(new GAME.Player(id,name,Games[chat].DICES));
		Games[chat].timeout = setTimeout( function() {
			if (Games[chat].players.length > 1) { send_dices("newgame_timeout") }
			else {
				SEND(_("Game hasn't started for lack of players. Create a new game"),[[{ text: _("New game"), callback_data: "/startgame" }]]);
				clearTimeout(Games[chat].timeout);
				delete Games[chat];
			}
		}, 30000);
		return _("You will receive your dices in a private chat");
	}

//	LEAVE COMMAND
	else if (text == "/leave" || text == "/leave@Perudobot") {
		if (Games[chat] == undefined) { return _("There is no game to leave. You can create a new one") }
		var ind = 0, l = Games[chat].players.length;
		while (ind < l && Games[chat].players[ind].id != id) { ind ++ }
		if (ind == l) { return _("You are not joined to the existent game") }
		if (Games[chat].playing) { player_leaves(ind,"<b>" + name + "</b> " + _("leaves the game")) }
		else {
			Games[chat].players.splice(ind,1);
			SEND("<b>" + name + "</b> " + _("leaves the game")) }
		return;
	}

//	JOIN COMMAND (ONLY FROM BUTTON)
	else if (is_answer && (text == "/join" || text == "/join@Perudobot")) {
		if (Games[chat] == undefined) { return "\u26d4 " + _("No games to join. You can create a new one") }
		var ingame = false;
		for (var i in Games[chat].players) { if (Games[chat].players[i].id == id) { ingame = true; break } }
		if (ingame) { return "\u26d4 " + _("You are already in the game") }
		if (Games[chat].playing) { return "\u26d4 " + _("A game has already started. Wait for it to be ended") }
		Games[chat].players.push(new GAME.Player(id,name,Games[chat].DICES));
		if (Games[chat].players.length < Games[chat].MAX_PLAYERS) {
			SEND("<b>" + name + "</b> " + _("joined the game"));
			return _("You will receive your dices in a private chat") }
		clearTimeout(Games[chat].timeout);
		send_dices("newgame_maxplayers",{ username: name });
		return _("You will receive your dices in a private chat");
	}

//	PALIFICO AND NOPALIFICO COMMAND (ONLY FROM BUTTON)
	else if (is_answer && (text == "/palifico" || text == "/palifico@Perudobot" || text == "/nopalifico" || text == "/nopalifico@Perudobot")) {
		if (Games[chat] == undefined) { return "\u26d4 " + _("You are not joined to any game. You can create a new one") }
		if (!Games[chat].waiting_palific) { return "\u26d4 " + _("You can't use Palifico now") }
		if (Games[chat].players[Games[chat].current].id != id) { return "\u26d4 " + _("It's not your turn") }
		clearTimeout(Games[chat].timeout);
		if (text == "/palifico" || text == "/palifico@Perudobot") { send_dices("palifico",{ username: Games[chat].players[Games[chat].current].username }) }
		else { send_dices("nopalifico",{ username: Games[chat].players[Games[chat].current].username }) }
		return;
	}

//	DOUBT COMMAND (ONLY FROM BUTTON)
	else if (is_answer && (text == "/doubt" || text == "/doubt@Perudobot")) {
		if (Games[chat] == undefined) { return "\u26d4 " + _("You are not joined to any game. You can create a new one") }
		var p=0; while (p < Games[chat].players.length) { if (Games[chat].players[p].id == id) { break } p++ } if (p == Games[chat].players.length) { return "\u26d4 " + _("You are not joined to the current game") }
		if (Games[chat].players[Games[chat].current].id != id) { return "\u26d4 " + _("It's not your turn") }
		if (Games[chat].bet[1] == 0) { return "\u26d4 " + _("You can't start doubting") }
		if (Games[chat].waiting_palific) { return "\u26d4 " + _("You have to choose between Palifico or not") }
		// Timeout is cleared
		clearTimeout(Games[chat].timeout);
		// Players reveal their dices and a dice is removed from the loser's hand
		var str = "<b>" + name + "</b> " + _("doubted. Players show their dices") + "\n\n";
		for (var i in Games[chat].players) {
			var player = Games[chat].players[i];
			str += "<b>" + player.username + "</b>: "
			for (var j in player.dices) { str += ICONS[player.dices[j]] }
			str += "\n";
		}
		var number = Games[chat].check();
		if (number < Games[chat].bet[0]) { Games[chat].current = (Games[chat].current - 1 + Games[chat].players.length) % Games[chat].players.length }
		var loser_ind = Games[chat].current;
		str += "\n" + _("There were") + " <b>" + number + "</b> " + ICONS[Games[chat].bet[1]] + ". <b>" + Games[chat].players[loser_ind].username + "</b> " + _("loses one dice");
		Games[chat].players[loser_ind].dices.pop();
		// The turn ends, depending on how many dices the loser holds
		if (Games[chat].players[loser_ind].dices.length == 0) { player_leaves(loser_ind,str + " " + _("and leaves the game for defeat")) }
		else if (Games[chat].players[loser_ind].dices.length > 1) { send_dices("doubt",{ str: str + "\n\n" }) }
		else {
			Games[chat].waiting_palific = true;
			str += " " + _("and can choose Palifico") + "\n\n" + _("The order of players is") + ":\n" + Games[chat].get_order();
			SEND(str,[[{ text: _("Palifico"), callback_data: "/palifico" },{ text: _("Not Palifico"), callback_data: "/nopalifico" }]]);
			if (Games[chat].BET_TIME > 0) { Games[chat].timeout = setTimeout( function() { send_dices("timeout_palifico",{ username: Games[chat].players[Games[chat].current].username }) }, Games[chat].BET_TIME) } }
		return;
	}

//	BET COMMAND
	else if (text[0] == "/") {
		var arr = text.split(" ");
		arr[0] = arr[0].substring(1);
		if (arr.length != 2) { return _("Invalid command") }
		if (!validate_number(arr[0]) || !validate_number(arr[1])) { return _("Invalid command") }
		if (Games[chat] == undefined) { return _("You are not joined to any game. You can create a new one") }
		var p=0; while (p < Games[chat].players.length) { if (Games[chat].players[p].id == id) { break } p++ } if (p == Games[chat].players.length) { return _("You are not joined to the current game") }
		if (Games[chat].players[Games[chat].current].id != id) { return _("It's not your turn") }
		if (Games[chat].waiting_palific) { return _("You have to choose between Palifico or not") }
		// Check if the bet is valid
		var bet = [parseInt(arr[0]),parseInt(arr[1])];
		var res = Games[chat].validate(bet);
		if (res > 0) { return ["ok",
			_("The number of dices on which you bet must be strictly positive"),
			_("The face of a dice must be between 1 and 6"),
			_("On Palifico you can't bet on a different face from the previous one"),
			_("You must increment the number of dices compared to the previous bet"),
			_("You can't start with a bet on") + " " + ICONS[1],
			_("If you bet on") + " " + ICONS[1] + " " + _("you must halve the number of dices rounding up, at least"),
			_("If you switch from") + " " + ICONS[1] + " " + _("to another face you must double the number of dices and sum one, at least"),
			_("You must increment the number of dices or the face compared to the previous bet"),
			_("If you want to bet again on") + " " + ICONS[1] + " " + _("you must increment the number of dices")
			][res] }
		// Clear the timeout and start a new turn
		clearTimeout(Games[chat].timeout);
		Games[chat].bet = bet;
		Games[chat].current = (Games[chat].current + 1) % Games[chat].players.length;
		SEND("<b>" + name + "</b> " + _("says that there are") + " <b>" + bet[0] + "</b> " + ICONS[bet[1]] + "\n<b>" + Games[chat].players[Games[chat].current].username + "</b> " + _("can raise or doubt"),[[{ text: _("Doubt"), callback_data: "/doubt" }]]);
		startTimer();
		return;
	}

	return;

}
